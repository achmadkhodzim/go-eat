class Order
attr_accessor :driver_name,:route,:store_name,:items

def initialize(driver_name,route,store_name,items)
@driver_name=driver_name
@route=route
@store_name=store_name
@items=items
@total_price=self.total_price
end

def total_price
total=0
@items.each do |menu|
total+=menu[1]*menu[2]
end
return total
end

end
