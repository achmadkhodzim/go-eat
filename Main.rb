require_relative 'User.rb'
require_relative 'Driver.rb'
require_relative 'Store.rb'
require_relative 'Order.rb'
require 'json'
$all_position=[[1,1]]
$items=[]

  $map_size=20
def find_nearest_driver(d_pos,indextoko)
  tempindex=[]
  d_pos.each do |isi|
  x=isi[0]-$toko[indextoko].position[0]
  y=isi[1]-$toko[indextoko].position[1]
isikan =x-y
  tempindex<< isikan.abs
  end
  return tempindex.find_index(tempindex.min)

end

def position_buffer
  random_position=[]
  loop do
    random_position=[rand($map_size),rand($map_size)]
   unless $all_position.include? random_position
     break
   end
  end
  $all_position<< random_position
  return random_position
end
def print_map(d_pos,t_pos,u_pos)

  $map_size.times do |x|
    $map_size.times do |y|
  if u_pos.include? [x,y]
    print "U"
  elsif d_pos.include? [x,y]
    print "D"
  elsif t_pos.include? [x,y]
    print "T"
  else
    print "#"
  end
    end
  puts ""
  end
end


def get_driver_position(drivers)
all_position=[]
drivers.each do |d|
all_position << d.position
end
return all_position
end
def get_store_position(toko)

  all_position=[]
toko.each do |t|
all_position << t.position
end
return all_position
end

# print_map(d_pos,t_pos,user.position)

def randomize_drivers
  drivers=[]
    drivers<< Driver.new("ojim",position_buffer)
    drivers<< Driver.new("agus",position_buffer)
    drivers<< Driver.new("adiv",position_buffer)
    drivers<< Driver.new("rizki",position_buffer)
    drivers<< Driver.new("rasyid",position_buffer)
    return drivers

end
def randomize_store
  toko = []
  toko<< Store.new("Giyok",[[10000,"ayam"],[2000,"es teh"]],position_buffer)
  toko<< Store.new("Andeska",[[50000,"ikan"],[2000,"es nutrisari"]],position_buffer)
  toko<< Store.new("Montekar",[[20000,"sate"],[2000,"es jeruk"]],position_buffer)
  return toko
end

def show_stores(toko)
puts "Daftar Toko di Go-Eat"
toko.each_with_index do |t,i|

  puts "#{i+1}. #{t.name}"

end

puts "Silahkan pilih toko yang ingin di order"
return gets.to_i-1
end
def show_menu(toko)
  jumlah=[]
  puts "Daftar Menu Toko #{toko.name}"
toko.menu.each_with_index do |m,i|
puts "#{i+1}. #{m[:nama]} Harga Rp. #{m[:harga]}"
end
puts "Masukkan Pilihan Menu"
pilihan=gets.to_i-1
puts "Anda Memilih #{toko.menu[pilihan][:nama]}"
puts "Jumlah"
jumlah=gets.to_i
puts "Total Harga#{jumlah*toko.menu[pilihan][:harga]}"
$items << [toko.menu[pilihan][:nama],jumlah,toko.menu[pilihan][:harga]]
puts "Apakah anda ingin mengorder lagi? y/t"

jawaban=gets.chomp
if jawaban=='y'
jawaban=-1
else
  jawaban=0
end
return jawaban
end

def main

if $drivers.empty?
puts "Dicari Driver Go-EAT!"
$drivers=randomize_drivers

end


  d_pos=get_driver_position($drivers)
  t_pos=get_store_position($toko)
menugoeat
x = gets.to_i
case x
when 1

  print_map(d_pos,t_pos,[$user.position])
  main
when 2
  indextoko=show_stores($toko)
  loop do
    x=show_menu($toko[indextoko])
   unless x==-1

     break
   end
   end
   indexdrive=find_nearest_driver(d_pos,indextoko)
   route=$drivers[indexdrive].move($toko[indextoko].position)
   $orders<< Order.new($drivers[indexdrive].name,route,$toko[indextoko].name,$items)
   file = File.open("orders.json","w+")
   hash =[]
   $orders.each do |orderan|
     hash << orderan.instance_variables.each_with_object({}) { |var, hash| hash[var.to_s.delete("@")] = orderan.instance_variable_get(var) }

   end

   file.puts JSON.pretty_generate(hash)
   file.close
   if $drivers[indexdrive].rating<3
     $drivers.delete_at(indexdrive)
   end
$items=[]
  main
when 3
  puts "History Order Anda"
  $orders.each_with_index do |order,i|
puts "#{i+1}. Order dari #{order.store_name} "
puts "Menu Order"
order.items.each_with_index do |menu,j|
puts "    #{j+1}. #{menu[0]} jumlah #{menu[1]} harga Rp. #{menu[2]}"
end
puts " total harga: Rp. #{order.total_price}"


  end
  main
else
  puts "Maaf input anda salah silahkan input lagi"
  main
end


end

def menuawal
  puts "Choose How to Execute"
  puts "1.No Arguments"
  puts "2.With 3 Args"
  puts "3.From File"
end

def menugoeat
  puts "Choose What to Do"
  puts "1.Show Map"
  puts "2.Order Food"
  puts "3.View History"


end

def realmain
$orders=[]
$drivers=randomize_drivers
$toko=randomize_store
$user = User.new("ojim",position_buffer)
$orders=[]
  menuawal
  choice = gets.to_i

case choice
when 1
  main
when 2
  puts "Input the argument"
  puts "First Argument (map size)"
  $map_size=gets.to_i
  $user = User.new("ojim",$map_size)

  puts "Second Argument (user x coordinate)"
  second=gets.to_i
  puts "Third Argument (user y coordinate)"
  third=gets.to_i
  $user.position=[second,third]
  $all_position << $user.position
  $drivers=randomize_drivers
  $toko=randomize_store
main
when 3
puts "input file name"
filename= gets.chomp
file= File.read(filename)
hash=JSON.parse(file)
$map_size= hash['map_size']
$user = User.new("ojim",$map_size)
$user.position=hash['user_position']
$all_position << $user.position
$drivers=[]
$toko=[]
hash['drivers_count'].times do
$drivers<< Driver.new("ojim",position_buffer)
end
hash['stores'].each do |loadtoko|
$toko << Store.new(loadtoko['name'],loadtoko['menu'],loadtoko['position'])

end

  main
else
  puts "Maaf Input anda salah silahkan input lagi"
  realmain
end
end


realmain
